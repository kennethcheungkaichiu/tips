<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .google-button {
                align-items: center;
                border: none;
                display: inline-flex;
                justify-content: center;
                outline: none;
                position: relative;
                z-index: 0;
                -webkit-font-smoothing: antialiased;
                background: none;
                border-radius: 4px;
                cursor: pointer;
                padding: 0 8px;
                white-space: pre-wrap;
            }

            /* ::before, which will become the hover effect */
            .google-button::before {
                    content: '';
                display: block;
                opacity: 0;
                position: absolute;
                transition-duration: .15s;
                transition-timing-function: cubic-bezier(0.4,0.0,0.2,1);
                z-index: -1;
                bottom: 0;
                left: 0;
                right: 0;
                top: 0;
                background: #4285f4;
                border-radius: 4px;
                transform: scale(0);
                transition-property: transform,opacity;
            }
        </style>
        @section('javascript') @parent <script> 
            $(document).ready(function() {
                $('.accordian-toggle').click(function() {
                    console.log("ads");
                    $('.panel-collapse').hide(); $(this).parent('.panel-heading').next().show();
                }); 
            });
        </script> @stop
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            

           <a class="google-button" href="/users">Tips Users</a>
           <a class="google-button" href="/tips">Tips</a>
           <a class="google-button" href="/getSchedule">REAL TIME FOOTBALL SCHEDULE</a>
           <a class="google-button" href="/getOdd">REAL TIME FOOTBALL ODD</a>
           <a class="google-button" href="/getResult">REAL TIME FOOTBALL RESULT</a>

        </div>
    </body>
</html>
