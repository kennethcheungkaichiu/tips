<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use DB;

class APIController
{

	public function _format_json($json, $html = false) {
		$tabcount = 0; 
		$result = ''; 
		$inquote = false; 
		$ignorenext = false; 
		if ($html) { 
		    $tab = "&nbsp;&nbsp;&nbsp;"; 
		    $newline = "<br/>"; 
		} else { 
		    $tab = "\t"; 
		    $newline = "\n"; 
		} 
		for($i = 0; $i < strlen($json); $i++) { 
		    $char = $json[$i]; 
		    if ($ignorenext) { 
		        $result .= $char; 
		        $ignorenext = false; 
		    } else { 
		        switch($char) { 
		            case '{': 
		                $tabcount++; 
		                $result .= $char . $newline . str_repeat($tab, $tabcount); 
		                break; 
		            case '}': 
		                $tabcount--; 
		                $result = trim($result) . $newline . str_repeat($tab, $tabcount) . $char; 
		                break; 
		            case ',': 
		                $result .= $char . $newline . str_repeat($tab, $tabcount); 
		                break; 
		            case '"': 
		                $inquote = !$inquote; 
		                $result .= $char; 
		                break; 
		            case '\\': 
		                if ($inquote) $ignorenext = true; 
		                $result .= $char; 
		                break; 
		            default: 
		                $result .= $char; 
		        } 
		    } 
		} 
		return $result; 
	}

	public function getScheduleString () {

	}


	public function getSchedule (Request $request) {


	   	$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://bet.hkjc.com/football/getJSON.aspx?jsontype=schedule.aspx",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: ",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
			if (strpos($response,'<HTML>') !== false) {
				$response = file_get_contents(base_path('resources/lang/schedule.json'));
				echo$this->_format_json($response,true);
			} else {
			  	echo $this->_format_json($response,true);

			  	file_put_contents(base_path('resources/lang/schedule.json'), $response);
			}
		}

	}

	public function getResult (Request $request) {

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://bet.hkjc.com/football/getJSON.aspx?jsontype=results.aspx",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: ",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  if (strpos($response,'<HTML>') !== false) {
				$response = file_get_contents(base_path('resources/lang/result.json'));
				echo$this->_format_json($response,true);
			} else {
			  	echo $this->_format_json($response,true);

			  	file_put_contents(base_path('resources/lang/result.json'), $response);
			}
		}

	}

	public function getOdd (Request $request) {

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_ilc.aspx",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: ",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  if (strpos($response,'<HTML>') !== false) {
				$response = file_get_contents(base_path('resources/lang/result.json'));
				echo$this->_format_json($response,true);
			} else {
			  	echo $this->_format_json($response,true);

			  	file_put_contents(base_path('resources/lang/result.json'), $response);
			}
		}

	}


	public function getUsers (Request $request) {

	}


	public function getTips (Request $request) {

	}


}