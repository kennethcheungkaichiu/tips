<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/users', 'APIController@getUsers');

Route::get('/tips', 'APIController@getTips');

Route::get('/getSchedule', 'APIController@getSchedule');

Route::get('/getOdd', 'APIController@getOdd');

Route::get('/getResult', 'APIController@getResult');